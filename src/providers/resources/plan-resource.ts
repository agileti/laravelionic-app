import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Env} from '../../models/env';
declare var ENV: Env;
/*
  Generated class for the PlanResourceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlanResourceProvider {

    constructor(public authHttp: AuthHttp) {
        console.log('Hello PlanResourceProvider Provider');
    }

    all(): Observable<Array<any>> {
        return this.authHttp.get(ENV.API_URL + '/planos').map(response => response.json().planos);
    }

}
