import {Injectable} from '@angular/core';
import {Request, BrowserXhr, ResponseOptions, XHRBackend, XSRFStrategy, XHRConnection} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {appContainer} from '../app/app.container';
import {JwtClient} from '../providers/jwt-client';
import {RedirectorProvider} from '../providers/redirector';

/*
  Generated class for the DefaultXhtrBackendProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DefaultXHRBackendProvider extends XHRBackend {

    constructor(browserXhr: BrowserXhr, baseResponseOptions: ResponseOptions, xsrfStrategy: XSRFStrategy) {
        super(browserXhr, baseResponseOptions, xsrfStrategy);
    }

    createConnection(request: Request): XHRConnection {
        let xhrConnection = super.createConnection(request);
        xhrConnection.response = xhrConnection.response.map(function (response) {
            this.tokenSetter(response);
            return response;
        }).catch(responseError => {
            this.onResponseError(responseError);
            return Observable.throw(responseError);
        });

        return xhrConnection;
    }

    tokenSetter(response: Response) {
        let jwtCliente = appContainer().get(JwtClient);
        if (response.headers.has('Authorization')) {
            let autorization = response.headers.get('Authorization');
            let token = autorization.replace('Bearer ', '');
            jwtCliente.setToken(token);
        }
    }

    onResponseError(responseError: Response) {
        let redirector = appContainer().get(RedirectorProvider);
        switch (responseError.status) {
            case 401:
                redirector.redirector();
                break;
            case 403:
                let data = responseError.json();
                let toHomePage = data.hasOwnProperty('error') && data.error == 'subscription_valid_not_found' ? true : false;
                redirector.redirector(toHomePage ? 'HomePage' : 'LoginPage');
                break;
        }
    }

}
