import {Injectable} from '@angular/core';
import {JwtClient} from '../providers/jwt-client';
import {JwtPayload} from '../models/jwt-payload';
import {JwtCredentials} from '../models/jwt-credentials';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {UserResourceProvider} from './resources/user-resource';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

    private _user = null;
    private _userSubject = new BehaviorSubject(null);
    
    constructor(public jwtClient: JwtClient, public fb: Facebook, public userResouce: UserResourceProvider) {
        this.user().then((user) => {
            console.log(user);
        });
    }
    
    userSubject(): BehaviorSubject<Object>{
        return this._userSubject;
    }

    user(): Promise<Object> {
        return new Promise((resolve) => {
            if (this._user) {
                resolve(this._user);
            } else {
                this.jwtClient.getPayload().then((payload: JwtPayload) => {
                    if (payload) {
                        this._user = payload.user;
                        this._userSubject.next(this._user);
                    }
                    resolve(this._user);
                });
            }
        });
    }

    check(): Promise<boolean> {
        return this.user().then(user => {
            return user !== null;
        });
    }

    login(jwtCredentials: JwtCredentials): Promise<Object> {
        return this.jwtClient.accessToken(jwtCredentials).then(() => {
            return this.user();
        });
    }

    logout() {
        return this.jwtClient.removeToken().then(() => {
            this._user = null;
            this._userSubject.next(this._user);
        });
    }

    loginFacebook(): Promise<Object> {
        return this.fb.login(['public_profile', 'user_friends', 'email'])
            .then((response: FacebookLoginResponse) => {
                let accessToken = response.authResponse.accessToken;
                return this.userResouce.register(accessToken).then(token => {
                    this.jwtClient.setToken(token);
                    return this.user;
                });
            }).catch(e => {
                //console.log('Error logging into Facebook', e);
                return '';
            });


        //this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
    }

}
