/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injector} from '@angular/core';

let localInjector:Injector;
export const appContainer = (injector?:Injector):Injector => {
    if(injector){
        localInjector = injector;
    }
    
    return localInjector;
}
