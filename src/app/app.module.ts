import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {Http, HttpModule, XHRBackend} from '@angular/http';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {LoginPage} from '../pages/login/login';
import {AdminPage} from '../pages/admin/admin';
import {AddCpfPage} from '../pages/add-cpf/add-cpf';
import {HomeSubscriberPage} from '../pages/home-subscriber/home-subscriber';
import {PaymentPage} from '../pages/payment/payment';
import {PlansPage} from '../pages/plans/plans';

import {IonicStorageModule} from '@ionic/storage';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Facebook} from '@ionic-native/facebook';

import {TextMaskModule} from 'angular2-text-mask'
import {AuthHttp, AuthConfig, JwtHelper} from 'angular2-jwt';

import { UserResourceProvider } from '../providers/resources/user-resource';
import { PlanResourceProvider } from '../providers/resources/plan-resource';
import {DefaultXHRBackendProvider} from '../providers/default-xhr-backend';
import {JwtClient} from '../providers/jwt-client';
import {AuthProvider} from '../providers/auth';
import {RedirectorProvider} from '../providers/redirector';

import {Env} from "../models/env";
declare var ENV: Env;

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AdminPage,
        AddCpfPage,
        HomeSubscriberPage,
        PaymentPage,
        PlansPage
    ],
    imports: [
        HttpModule,
        BrowserModule,
        TextMaskModule,
        IonicModule.forRoot(MyApp, {}, {
            links: [
                {component: LoginPage, name: 'LoginPage', segment: 'login'},
                {component: HomePage, name: 'HomePage', segment: 'home'},
                {component: AdminPage, name: 'AdminPage', segment: 'admin'},
                {component: AddCpfPage, name: 'AddCpfPage', segment: 'addcpf'},
                {component: HomeSubscriberPage, name: 'HomeSubscriberPage', segment: 'subscriber/home'},
                {component: PaymentPage, name: 'PaymentPage', segment: 'plan/:plan/payment'},
                {component: PlansPage, name: 'PlansPage', segment: 'plans'}
            ]
        }),
        IonicStorageModule.forRoot({
            driverOrder: ['localstorage']
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        LoginPage,
        AddCpfPage,
        HomeSubscriberPage,
        PaymentPage,
        PlansPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        JwtClient,
        JwtHelper,
        Facebook,
        AuthProvider,
        RedirectorProvider,
        UserResourceProvider,
        PlanResourceProvider,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {
            provide: AuthHttp, deps: [Http, IonicStorageModule], useFactory(http, storage) {
                let authConfig = new AuthConfig({
                    headerPrefix: 'Bearer',
                    noJwtError: true,
                    noClientCheck: true,
                    tokenGetter: (() => storage.get(ENV.TOKEN_NAME))
                });
                return new AuthHttp(authConfig, http);
            }
        },
        {provide: XHRBackend, useClass: DefaultXHRBackendProvider},    
    ]
})
export class AppModule {}
