import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Auth} from '../../decorators/auth.decorator';
import {UserResourceProvider} from '../../providers/resources/user-resource';

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Auth()
@IonicPage()
@Component({
    selector: 'page-admin',
    templateUrl: 'admin.html',
})
export class AdminPage {

    user = {
        'password': '',
        'password_confirmation': '',
    };

    constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public userResource: UserResourceProvider) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AdminPage');
    }

    submit() {
        let toast = this.toastCtrl.create({
            duration: 3000,
            position: 'top'
        });

        this.userResource.updatePassword(this.user).then(() => {
            toast.setMessage('Dados Salvos com Sucesso');
            toast.present();
        }).catch(() => {
            toast.setMessage('Dados inválidos, tente novamente');
            toast.present();
        });
    }

}
