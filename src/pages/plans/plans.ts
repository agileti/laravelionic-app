import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {PlanResourceProvider} from '../../providers/resources/plan-resource'
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/**
 * Generated class for the PlansPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-plans',
    templateUrl: 'plans.html',
})
export class PlansPage {

    plans: Observable<Array<Object>>;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public planResourceProvider: PlanResourceProvider,
        public loadingCtrl: LoadingController
    ) {
    }

    ionViewDidLoad() {
        let loading = this.loadingCtrl.create({
            content: 'carregando planos...'
        });
        loading.present();
        
        this.plans = this.planResourceProvider.all().map(plans => {
            loading.dismiss();
            return plans;
        });
    }

}
