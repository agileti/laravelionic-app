import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Auth} from '../../decorators/auth.decorator';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/toPromise';

@Auth()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController, public authHttp: AuthHttp) {
        
    }
    
    ionViewDidLoad(){
        this.authHttp.get('http://laravelionic/api/user').toPromise().then(()=>{
            console.log('primeira ')
        });
    }

}
