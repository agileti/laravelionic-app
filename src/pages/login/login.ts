import {Component} from '@angular/core';
import {IonicPage, NavController, MenuController, ToastController, NavParams} from 'ionic-angular';
import {AuthProvider} from '../../providers/auth';
import "rxjs/add/operator/toPromise";

import {HomePage} from '../home/home';
import {HomeSubscriberPage} from '../home-subscriber/home-subscriber';

/**
 * Generated class for the Login page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    user = {
        email: "contato@agileti.com.br",
        password: "123456"
    };

    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public navParams: NavParams,
        public toastCtrl: ToastController,
        private auth: AuthProvider
    ) {
        this.menuCtrl.enable(false);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    login() {
        this.auth.login(this.user).then((user) => {
            this.afterLogin(user);
        }).catch(() => {
            let toast = this.toastCtrl.create({
                message: 'Email e/ou Senha inválidos',
                duration: 3000,
                position: 'top'
            });

            toast.present();
        });
    }

    afterLogin(user) {
        this.menuCtrl.enable(true);
        this.navCtrl.push(user.subscription_valid ? HomeSubscriberPage : HomePage);
    }

    loginFacebook() {
        this.auth.loginFacebook().then((user) => {
            this.afterLogin(user);
        }).catch(() => {
            let toast = this.toastCtrl.create({
                message: 'Erro ao realizar login no Facebook.',
                duration: 3000,
                position: 'top'
            });

            toast.present();
        });
    }

}
