import {Component} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the HomeSubscriberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-home-subscriber',
    templateUrl: 'home-subscriber.html',
})
export class HomeSubscriberPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public authHttp: AuthHttp) {
    }

    ionViewDidLoad() {
        this.authHttp.get('http://localhost:8100/api/test').toPromise().then(() => {
            
        });
    }

}
