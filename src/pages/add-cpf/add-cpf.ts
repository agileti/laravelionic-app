import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {UserResourceProvider} from '../../providers/resources/user-resource'

/**
 * Generated class for the AddCpfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-add-cpf',
    templateUrl: 'add-cpf.html',
})
export class AddCpfPage {
    cpf = null;
    mask = [
        /\d/, /\d/, /\d/, '.',
        /\d/, /\d/, /\d/, '.',
        /\d/, /\d/, /\d/, '-',
        /\d/, /\d/
    ];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public userResource: UserResourceProvider,
        public toastCtrl: ToastController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddCpfPage');
    }

    submit() {
        this.userResource.addCpf(this.cpf).then(() => {
            this.navCtrl.push('PlansPage');
        }).catch(() => {
            let toast = this.toastCtrl.create({
                message: 'CPF Inválido! Verifique novamente',
                duration: 3000,
                position: 'top'
            });

            toast.present();
        });
    }

}
